using UnityEngine;

public class LevelService : MonoBehaviour
{
    private const string LevelKey = "LevelKey";
    
    [SerializeField] private Level[] levels;

    public void StartGame()
    {
        int levelIndex = GetLevel();
        Level level = levels[levelIndex % levels.Length];
        if (levelIndex == 0)
        {
            LevelContainer.Resolve<GameController>().Tutorial(level);
        }
        else
        {
            LevelContainer.Resolve<GameController>().NewGame(level);
        }
    }

    private int GetLevel()
    {
        int level = PlayerPrefs.GetInt(LevelKey, 0);
        return level;
    }

    public void AddLevel()
    {
        PlayerPrefs.SetInt(LevelKey, GetLevel() + 1);
    }
}
