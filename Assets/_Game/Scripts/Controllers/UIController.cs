using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] private TutorialScreen tutorialScreen;
    [SerializeField] private GameObject startScreen;
    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private GameObject gameWonScreen;

    private GameController _gameController;
    public void Init()
    {
        _gameController = LevelContainer.Resolve<GameController>();
        
        _gameController.OnNewGame += HandleNewGame;
        _gameController.OnGameStart += HandleGameStart;
        _gameController.OnGameWon += HandleGameWon;
        _gameController.OnGameOver += HandleGameOver;
        _gameController.OnTutorialStarted += HandleTutorialStarted;
    }

    public void OnnClickStart()
    {
        _gameController.StartGame();
    }
    
    public void OnClickContinue()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); // restart yazmaya zaman ayıramadım
    }

    private void HandleNewGame(Level level)
    {
        startScreen.SetActive(true);
    }

    private void HandleTutorialStarted()
    {
        startScreen.SetActive(false);
        tutorialScreen.gameObject.SetActive(true);
        tutorialScreen.StartTutorial();
    }

    private void HandleGameStart()
    {
        startScreen.SetActive(false);
    }

    private void HandleGameWon()
    {
        LevelContainer.Resolve<LevelService>().AddLevel();
        gameWonScreen.SetActive(true);
    }

    private void HandleGameOver()
    {
        gameOverScreen.SetActive(true);
    }
}
