using System;
using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public Action<Level> OnNewGame; 
    public Action OnGameStart, OnGameWon, OnGameOver, OnTutorialStarted;
    public void Init()
    {
        
    }

    public void Tutorial(Level level)
    {
        NewGame(level);
        OnTutorialStarted?.Invoke();
    }
    public void NewGame(Level level)
    {
        OnNewGame?.Invoke(level);
    }

    public void StartGame()
    {
        OnGameStart?.Invoke();
    }

    public void GameWon()
    {
        OnGameWon?.Invoke();
    }

    public void GameOver()
    {
        OnGameOver?.Invoke();
    }
}
