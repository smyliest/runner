using System;
using UnityEngine;

public class WeaponService : MonoBehaviour
{
    [SerializeField] private WeaponSettings[] weapons;

    public Action OnWeaponUpgrade;

    public void UpgradeWeapon()
    {
        OnWeaponUpgrade?.Invoke();
    }
    public WeaponSettings GetWeapon(int tier)
    {
        return weapons[Mathf.Min(tier, weapons.Length - 1)];
    }
}
