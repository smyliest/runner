using DG.Tweening;
using UnityEngine;

public class TutorialScreen : MonoBehaviour
{
    [SerializeField] private RectTransform hand;
    [SerializeField] private AnimationCurve curve;

    private Tween _tween;
    public void StartTutorial()
    {
        LevelContainer.Resolve<GameController>().OnGameStart += HandleGameStarted;
        Vector2 pos = hand.anchoredPosition;
        _tween = DOVirtual.Float(0, 1, 2, value =>
        {
            pos.x = Mathf.LerpUnclamped(0, 200, curve.Evaluate(value));
            hand.anchoredPosition = pos;
        }).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
    }

    void HandleGameStarted()
    {
        _tween.Kill();
        gameObject.SetActive(false);
    }
}
