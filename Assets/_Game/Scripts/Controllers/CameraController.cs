using UnityEditor;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private PlayerController _playerContoller;
    
    private Vector3 _offset;
    private float _xPosition;
    private Vector3 _tempPosition;
    public void Init()
    {
        _playerContoller = LevelContainer.Resolve<PlayerController>();
        _playerContoller.OnTick += Tick;
        _offset = transform.position - _playerContoller.transform.position;
        _xPosition = transform.position.x;
    }

    private void Tick()
    {
        _tempPosition = _playerContoller.transform.position + _offset;
        _tempPosition.x = _xPosition;
        transform.position = _tempPosition;
    }
}
