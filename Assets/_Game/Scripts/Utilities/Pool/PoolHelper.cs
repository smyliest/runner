using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class PoolHelper
{
    private static Dictionary<GameObject, Pool> _pools;

    public static void Init()
    {
        _pools = new Dictionary<GameObject, Pool>();
    }

    public static void SetUp(GameObject template)
    {
        if (!_pools.Keys.Contains(template))
        {
            _pools.Add(template, new Pool(template));
        }
    }

    public static T Get<T>(this GameObject obj)
    {
        return Get(obj).GetComponent<T>();
    }
    public static GameObject Get(this GameObject template)
    {
        return _pools[template].Get();
    }

    public static void Recycle(this GameObject obj)
    {
        obj.GetComponent<Drop>().Pool.Recycle(obj);
    }
}
