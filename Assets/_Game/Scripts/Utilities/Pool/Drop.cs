using UnityEngine;

public class Drop : MonoBehaviour
{
    public Pool Pool => _pool;
    private Pool _pool;

    public void SetPool(Pool pool)
    {
        _pool = pool;
    }
}
