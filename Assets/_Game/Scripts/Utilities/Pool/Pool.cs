using System.Collections.Generic;
using UnityEngine;

public class Pool
{
    private GameObject _template;

    private List<GameObject> _pool;
    public Pool(GameObject template)
    {
        _template = template;
        _pool = new List<GameObject>();
    }

    private GameObject SpawnNew()
    {
        GameObject obj = GameObject.Instantiate(_template);
        Drop drop = obj.AddComponent<Drop>();
        drop.SetPool(this);
        return obj;
    }
    public GameObject Get()
    {
        if (_pool.Count == 0)
        {
            _pool.Add(SpawnNew());
        }

        GameObject obj = _pool[0];
        _pool.Remove(obj);
        obj.SetActive(true);
        return obj;
    }

    public void Recycle(GameObject obj)
    {
        obj.SetActive(false);
        obj.transform.SetParent(null);
        obj.transform.localScale = Vector3.zero;
        _pool.Add(obj);
    }
}
