using UnityEngine;

public class MuzzleHolder : MonoBehaviour
{
    public Transform Muzzle => muzzle;
    [SerializeField] private Transform muzzle;
}
