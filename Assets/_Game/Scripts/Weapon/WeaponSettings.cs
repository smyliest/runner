using UnityEngine;

[CreateAssetMenu]
public class WeaponSettings : ScriptableObject
{
    public GameObject BulletModel => bulletModel;
    public GameObject Model => model;
    public int BurstCount => burstCount;
    public float CoolDownTime => coolDownTime;
    public float BurstInterval => burstInterval;

    [SerializeField] private GameObject bulletModel;
    [SerializeField] private GameObject model;
    [SerializeField] private int burstCount;
    [SerializeField] private float coolDownTime;
    [SerializeField] private float burstInterval;
}
