using System;
using DG.Tweening;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public Vector3 Direction => _direction; 
    public int Damage => _damage;
    
    private Vector3 _direction;
    private int _damage;
    public void Fire(Vector3 direction)
    {
        _direction = direction;
        _damage = 1; //farklı silahlar icin farklı deger girilebilir
        
        transform.localScale = Vector3.one * .2f;
        transform.DOKill();
        transform.DOMove(_direction * 300, 10).SetRelative().SetEase(Ease.Linear).onComplete = () =>
        {
            transform.DOKill();
            gameObject.Recycle();
        };
    }

    
    private void OnTriggerEnter(Collider other)
    {
        other.transform.GetComponent<EnemyTransform>().Enemy.HandleHit(this);
        transform.DOKill();
        gameObject.Recycle();
    }
}
