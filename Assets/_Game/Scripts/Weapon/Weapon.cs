using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEditor;

public class Weapon
{
    private GameObject _bulletTemplate;
    private WeaponSettings _settings;
    private Transform _muzzle;
    private GameObject _model;
    private float _coolDown;
    private int _burst;
    private WaitForSeconds _wait;
    private float _lastFireTime;
    private float _burstInterval;
    public Weapon(WeaponSettings settings, Transform hand)
    {
        _settings = settings;
        _coolDown = settings.CoolDownTime;
        _bulletTemplate = settings.BulletModel;
        _burst = settings.BurstCount;
        _burstInterval = settings.BurstInterval;
        
        PoolHelper.SetUp(_bulletTemplate);

        _model = GameObject.Instantiate(settings.Model, hand);
        _model.transform.localPosition = Vector3.zero;
        _model.transform.localScale = Vector3.one;
        _model.transform.localRotation = Quaternion.identity;

        _muzzle = _model.GetComponent<MuzzleHolder>().Muzzle;

        _wait = new WaitForSeconds(settings.BurstInterval);
    }


    public void TryFire()
    {
        if (Time.time - _lastFireTime >= _coolDown)
        {
            Fire();
        }
    }

    
    private void Fire()
    {
        _lastFireTime = Time.time;

        for (int i = 0; i < _burst; i++)
        {
            DOVirtual.DelayedCall(i * _burstInterval, () =>
            {
                SingleFire();
            });
        }
    }
    


    private void SingleFire()
    {
        Bullet bullet = _bulletTemplate.Get<Bullet>();

        bullet.transform.position = _muzzle.position;
        bullet.Fire(_model.transform.forward);
    }
    public void Destroy()
    {
        GameObject.Destroy(_model);
    }
}
