using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    [SerializeField] private Platform platform;
    [SerializeField] private GameObject[] templates;
    [SerializeField] private Material blueMaterial, whiteMaterial;

    private Level _level;
    private Dictionary<string, GameObject> templateDictionary;


    public void Init()
    {
        LevelContainer.Resolve<GameController>().OnNewGame += HandleNewGame;
    }
    private void HandleNewGame(Level level)
    {
        _level = level;
        Build();
    }

    void Build()
    {
        float maxDistance = 0
            ;
        templateDictionary = new Dictionary<string, GameObject>();
        foreach (var template in templates)
        {
            PoolHelper.SetUp(template);
            templateDictionary.Add(template.name, template);
        }
        
        List<EnemyModel> enemies = DataCreator.ReadData(_level.LevelData);

        foreach (var enemy in enemies)
        {
            Transform t = templateDictionary[enemy.modelName].Get().transform;
            
            Enemy e;
            Material m;
            if (enemy.type == EnemyType.Kill)
            {
                e = new KillableEnemy();
                m = whiteMaterial;
            }
            else
            {
                e = new KnockableEnemy();
                m = blueMaterial;
            }
            e.SetUp(enemy, t, m);

            maxDistance = Mathf.Max(maxDistance, enemy.position.z);
        }
        Resize(maxDistance);

        platform.SetDoorPositions(_level.Door1, _level.Door2);
    }
    void Resize(float maxDistance)
    {
        int blocks = Mathf.CeilToInt(maxDistance / 5f) + 2;
        platform.SetSize(blocks * 5);
    }
}
