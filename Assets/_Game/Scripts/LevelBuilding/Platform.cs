using UnityEngine;

public class Platform : MonoBehaviour
{
    
    
    [SerializeField] private Transform floor, holder, door1, door2;
    [SerializeField] private Material floorMaterial;
    [SerializeField] private Transform finishLine;

    private const float Width = 5;

    public void SetSize(float lenght)
    {
        floor.localScale = new Vector3(Width, lenght, 1);
        floor.localPosition = (lenght - Width) * .5f * Vector3.forward;
        floor.localPosition += Vector3.down;
        floorMaterial.SetTextureScale("_MainTex", new Vector2(1, lenght / Width));
        finishLine.localPosition = (lenght - Width - 2.5f) * Vector3.forward + Vector3.down * .99f;
    }

    public void SetDoorPositions(Vector3 pos1, Vector3 pos2)
    {
        door1.localPosition = pos1;
        door2.localPosition = pos2;
    }
}
