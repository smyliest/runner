
using UnityEngine;

[CreateAssetMenu]
public class Level : ScriptableObject
{
    public string LevelData => levelData.text;

    public Vector3 Door1 => door1;
    public Vector3 Door2 => door2;
    
    [SerializeField] private TextAsset levelData;
    [SerializeField] private Vector3 door1;
    [SerializeField] private Vector3 door2;
}
