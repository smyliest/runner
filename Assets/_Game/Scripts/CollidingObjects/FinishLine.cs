using UnityEngine;

public class FinishLine : CollidingObject
{
    protected override void OnTouch(Transform other)
    {
        LevelContainer.Resolve<GameController>().GameWon();
    }
}
