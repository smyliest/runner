using UnityEngine;

public class CollidingObject : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 6) // bu biraz cirkin oldu 
        {
            OnTouch(other.transform);
        }
    }

    protected virtual void OnTouch(Transform other)
    {
        
    }
}
