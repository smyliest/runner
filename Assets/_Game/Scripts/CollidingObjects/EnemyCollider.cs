using UnityEngine;

public class EnemyCollider : CollidingObject
{
    protected override void OnTouch(Transform other)
    {
        LevelContainer.Resolve<GameController>().GameOver();
    }
}
