using UnityEngine;

public class WeaponDoor : CollidingObject
{
    protected override void OnTouch(Transform other)
    {
        LevelContainer.Resolve<WeaponService>().UpgradeWeapon();
    }
}
