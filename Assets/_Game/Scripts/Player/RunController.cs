using UnityEngine;

public class RunController : Ticker
{
    private const float Speed = 3;
    
    private InputController _inputController;
    private Vector3 _position;
    private Transform _transform;
    public RunController(InputController inputController, Transform transform)
    {
        _inputController = inputController;
        _transform = transform;
    }
    public void Tick()
    {
        _position = _transform.position;
        _position.x += _inputController.Position;
        _position.z += Speed * Time.deltaTime;
        if (_position.x > 2.5f)
        {
            _position.x = 2.5f;
        }

        if (_position.x < -2.5f)
        {
            _position.x = -2.5f;
        }
        _transform.position = _position;
    }
}
