
using UnityEngine;

public class WeaponController : Ticker
{

    private WeaponService _weaponService;
    private Transform _hand;
    private WeaponSettings _settings;
    private Weapon _weapon;
    private int _currentTier;
    public WeaponController(Transform hand)
    {
        _hand = hand;
        _currentTier = 0;
        _weaponService = LevelContainer.Resolve<WeaponService>();
        UpgradeTier();
        _weaponService.OnWeaponUpgrade += HandleWeaponUpgrade;
    }

    private void HandleWeaponUpgrade()
    {
        _currentTier++;
        UpgradeTier();
    }

    private void DestroyWeapon()
    {
        if (_weapon != null)
        {
            _weapon.Destroy();
        }
    }
    public void UpgradeTier()
    {
        DestroyWeapon();
        _settings = _weaponService.GetWeapon(_currentTier);
        _weapon = new Weapon(_settings, _hand);
    }
    public void Tick()
    {
        _weapon.TryFire();
    }
}
