using System;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public Action OnTick;

    [SerializeField] private Transform hand;

    private GameController _gameController;
    
    private InputController _inputController;
    private RunController _runController;
    private WeaponController _weaponController;
    private Coroutine _coroutine;


    private bool _running;
    public void Init()
    {
        _gameController = LevelContainer.Resolve<GameController>();
        _gameController.OnNewGame += HandleNewGame;
        _gameController.OnGameStart += HandleGameStart;
        _gameController.OnGameWon += HandleGameWon;
        _gameController.OnGameOver += HandleGameOver;

        _inputController = new PlayerInputController();
        // _inputController = new AIInputController(); ai istersek
        // _inputController = new RemoteInputController(); remote istersek
        
        _runController = new RunController(_inputController, transform);
        _weaponController = new WeaponController(hand);
        
    }

    void HandleNewGame(Level level)
    {
        
    }

    void HandleGameStart()
    {
        Enable();
        _inputController.Tick();
    }

    void HandleGameWon()
    {
        Disable();
    }

    void HandleGameOver()
    {
        Disable();
    }
    
    
    private void Enable()
    {
        _running = true;
        StartCoroutine(Ticking());
    }

    private void Disable()
    {
        _running = false;
        // if (_coroutine != null)
        // {
        //     StopCoroutine(_coroutine);
        // }
    }
    private void Tick()
    {
        _inputController.Tick();
        _runController.Tick();
        _weaponController.Tick();
        
        OnTick?.Invoke();
    }

    private IEnumerator Ticking()
    {
        while (_running)
        {
            yield return null;
            Tick();
        }
    }
}
