using UnityEngine;

public class PlayerInputController : InputController //belki input u ai ya da remote a donustururuz diye
{
    private float _lastPosition;
    public override void Tick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnTouchStarted();
        }

        if (Input.GetMouseButton(0))
        {
            OnTouch();
        }

        if (Input.GetMouseButtonUp(0))
        {
            OnTouchEnded();
        }
    }

    void OnTouchStarted()
    {
        _lastPosition = GetTouch();
    }
    void OnTouch()
    {
        _position = GetTouch() - _lastPosition;
        _lastPosition = GetTouch();
    }

    void OnTouchEnded()
    {
        _position = 0;
    }

    float GetTouch()
    {
        return Camera.main.ScreenToViewportPoint(Input.mousePosition).x * 5;// todo buna bi bakılacak
    }
}
