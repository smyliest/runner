using System;
using System.Collections.Generic;
using UnityEngine;

public class ServiceLocator
{
    private Dictionary<Type, object> _services;
    public ServiceLocator()
    {
        _services = new Dictionary<Type, object>();
    }

    public T Resolve<T>()
    {
        return (T) _services[typeof(T)];
    }

    public void RegisterMono<T>(Transform parent = null) where T : MonoBehaviour
    {
        T mono;
        if (parent == null)
        {
            mono = GameObject.FindObjectOfType<T>();
        }
        else
        {
            mono = parent.gameObject.GetComponentInChildren<T>();
        }
        Register<T>(mono);
    }

    public T Register<T>(object service)
    {
        _services.Add(typeof(T), service);
        return (T) service;
    }
    public T Register<T>() where T : new()
    {
        var serviceInstance = new T();
        Register<T>(serviceInstance);
        
        return serviceInstance;
    }

}
    
    
