using System.Diagnostics;
using UnityEngine;

public class Container : MonoBehaviour
{
    public ServiceLocator ServiceLocator => _serviceLocator;

    protected static Container _Instance;
    
    private ServiceLocator _serviceLocator;

    private void Awake()
    {
        Install();
    }

    protected virtual void Install()
    {
        _Instance = this;
        _serviceLocator = new ServiceLocator();
    }

    public static void RegisterMono<T>(Transform parent = null) where T : MonoBehaviour
    {
        _Instance._serviceLocator.RegisterMono<T>(parent);
    }

    public static void Register<T>(object service)
    {
        _Instance._serviceLocator.Register<T>(service);
    }

    public static void Register<T>() where T : new()
    {
        _Instance._serviceLocator.Register<T>();
    }

    public static T Resolve<T>()
    {
        return _Instance._serviceLocator.Resolve<T>();
    }
}
