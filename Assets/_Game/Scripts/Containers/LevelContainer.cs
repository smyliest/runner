
public class LevelContainer : Container
{
    protected override void Install()
    {
        base.Install();
        PoolHelper.Init();
        ServiceLocator.RegisterMono<GameController>();
        ServiceLocator.RegisterMono<PlayerController>();
        ServiceLocator.RegisterMono<WeaponService>();
        ServiceLocator.RegisterMono<HealthViewService>();
        ServiceLocator.RegisterMono<CameraController>();
        ServiceLocator.RegisterMono<UIController>();
        ServiceLocator.RegisterMono<LevelService>();
        ServiceLocator.RegisterMono<LevelBuilder>();
        
        
        
        ServiceLocator.Resolve<GameController>().Init();
        ServiceLocator.Resolve<PlayerController>().Init();
        ServiceLocator.Resolve<HealthViewService>().Init();
        ServiceLocator.Resolve<CameraController>().Init();
        ServiceLocator.Resolve<UIController>().Init();
        ServiceLocator.Resolve<LevelBuilder>().Init();
        
        ServiceLocator.Resolve<LevelService>().StartGame();
        
        
    }
}
