using System;

public class Health
{
    public Action OnHealthChanged;
    public Action OnDeath;
    
    public int CurrentHealth => _currentHealth;
    public int MaxHealth => _maxHealth;
    
    private int _currentHealth;
    private int _maxHealth;

    public Health(int maxHealth)
    {
        _maxHealth = maxHealth;
        _currentHealth = maxHealth;
    }

    public void DealDamage(int amount)
    {
        _currentHealth -= amount;
        OnHealthChanged?.Invoke();

        if (_currentHealth <= 0)
        {
            Kill();
        }
    }

    private void Kill()
    {
        OnDeath?.Invoke();
    }
}
