using UnityEngine;

public class EnemyTransform : MonoBehaviour
{
    public Enemy Enemy => _enemy;

    private Enemy _enemy;

    public void SetEnemy(Enemy enemy)
    {
        _enemy = enemy;
    }
}
