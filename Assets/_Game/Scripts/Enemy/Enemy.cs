using UnityEngine;

public class Enemy
{

    protected Rigidbody _rigidbody;
    protected Transform _transform;
    public virtual void SetUp(EnemyModel model, Transform transform, Material material)
    {
        _transform = transform;
        _transform.localPosition = model.position;
        _transform.rotation = Quaternion.Euler(model.rotation);
        _transform.localScale = model.scale;
        transform.GetComponent<MeshRenderer>().material = material;
        transform.GetComponent<EnemyTransform>().SetEnemy(this);
        
        _rigidbody = transform.GetComponent<Rigidbody>(); 
        if (_rigidbody == null)//enemy leri ilerde pool a alırsak tekrar eklemeyelim diye, rigidbody settings leri ozellestirmek istersek enemymodel class ına eklenebilir
        {
            _rigidbody = transform.gameObject.AddComponent<Rigidbody>();
        }

        EnemyCollider col = transform.GetComponent<EnemyCollider>();
        if (col == null)
        {
            transform.gameObject.AddComponent<EnemyCollider>();
        }

    }
    public virtual void HandleHit(Bullet bullet)
    {
        
    }
}
