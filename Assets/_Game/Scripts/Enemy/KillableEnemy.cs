using UnityEngine;
using UnityEngine.PlayerLoop;

public class KillableEnemy : Enemy
{

    private Health _health;
    private HealthView _healthView;
    private HealthViewService _viewService;
    public override void SetUp(EnemyModel model, Transform transform, Material material)
    {
        base.SetUp(model, transform, material);
        // _health = new Health(model.Health);
        _health = new Health(Random.Range(2,4));
        _health.OnDeath += HandleDeath;
        _viewService = LevelContainer.Resolve<HealthViewService>();
        _healthView = _viewService.Get();
        _healthView.SetUp(_health);
        _healthView.transform.position = _transform.position + Vector3.up;
    }
    public override void HandleHit(Bullet bullet)
    {
        _health.DealDamage(bullet.Damage);
    }

    private void HandleDeath()
    {
        _transform.gameObject.Recycle();
        _viewService.Return(_healthView);
    }
}
