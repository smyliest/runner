using UnityEngine;

public class KnockableEnemy : Enemy
{
    public override void SetUp(EnemyModel model, Transform transform, Material material)
    {
        base.SetUp(model, transform, material);
    }

    public override void HandleHit(Bullet bullet)
    {
        _rigidbody.AddForce(bullet.Direction * 300);
    }
}