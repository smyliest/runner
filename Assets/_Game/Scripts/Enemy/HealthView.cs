using UnityEngine;
using UnityEngine.UI;

public class HealthView : MonoBehaviour
{
    [SerializeField] private Text text;

    private Health _health;
    public void SetUp(Health health)
    {
        _health = health;
        _health.OnHealthChanged += HandleHealthChanged;
        HandleHealthChanged();
    }

    void HandleHealthChanged()
    {
        text.text = _health.CurrentHealth.ToString();
    }
}
