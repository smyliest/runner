using UnityEngine;

public class EnemyModel
{
    private const char VectorSeparator = '*';
    private const char FieldSeparator = '|';

    public int Health;
    public EnemyType type;
    public string modelName;
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;

    
    public void ReadFromTransform(Transform transform)
    {
        if (transform.CompareTag("kill"))
        {
            type = EnemyType.Kill;
        }
        else
        {            
            type = EnemyType.Knock;
        }
        modelName = transform.name;
        position = transform.localPosition;
        rotation = transform.rotation.eulerAngles;
        scale = transform.localScale;
    }
    public void ReadFromData(string data)
    {
        string[] fields = data.Split(FieldSeparator);
        if (fields[0] == "Kill")
        {
            type = EnemyType.Kill;
        }
        else
        {
            type = EnemyType.Knock;
        }

        modelName = fields[1];

        position = DataToVector(fields[2]);
        rotation = DataToVector(fields[3]);
        scale = DataToVector(fields[4]);
    }
    public string ToData()
    {
        string result = "";

        result += type.ToString() + FieldSeparator;
        result += modelName + FieldSeparator;
        result += VectorToData(position) + FieldSeparator;
        result += VectorToData(rotation) + FieldSeparator;
        result += VectorToData(scale);
        
        return result;
    }

    Vector3 DataToVector(string data)
    {
        Vector3 result = Vector3.zero;
        string[] values = data.Split(VectorSeparator);

        result.x = float.Parse(values[0]);
        result.y = float.Parse(values[1]);
        result.z = float.Parse(values[2]);
        return result;
    }
    string VectorToData(Vector3 vector)
    {
        return vector.x.ToString() + VectorSeparator + vector.y.ToString() + VectorSeparator + vector.z.ToString();
    }
}

public enum  EnemyType {Kill, Knock}
