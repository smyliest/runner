using UnityEngine;

public class HealthViewService : MonoBehaviour
{
    [SerializeField] private GameObject template;
    [SerializeField] private Transform canvas;

    public void Init()
    {
        PoolHelper.SetUp(template);
    }

    public HealthView Get()
    {
        Transform view = template.Get().transform;
        view.SetParent(canvas);
        view.transform.localScale = Vector3.one * .1f;
        return view.GetComponent<HealthView>();
    }

    public void Return(HealthView view)
    {
        view.gameObject.Recycle();
    }
}
