using System.Collections.Generic;
using UnityEngine;

public class DataCreator : MonoBehaviour
{
    private List<EnemyModel> enemies;
    
    
    void Start()
    {
        PoolHelper.Init();
        enemies = new List<EnemyModel>();

        for (int i = 0; i < transform.childCount; i++)
        {
            EnemyModel model = new EnemyModel();
            model.ReadFromTransform(transform.GetChild(i));
            enemies.Add(model);
        }

        string data = CreateData();
        Debug.Log(data);

    }

    private string CreateData()
    {
        string result = "";
        int count = 0;
        foreach (var enemy in enemies)
        {
            result += enemy.ToData();
            if(count != enemies.Count - 1) result += "\n";
            count++;
        }

        return result;
    }

    public static List<EnemyModel> ReadData(string data)
    {
        List<EnemyModel> enemies = new List<EnemyModel>();

        string[] array = data.Split('\n');

        
        foreach (var d in array)
        {
            // Debug.Log(d);
            EnemyModel model = new EnemyModel();
            model.ReadFromData(d);
            enemies.Add(model);
        }
        return enemies;
    }
    
    

}
